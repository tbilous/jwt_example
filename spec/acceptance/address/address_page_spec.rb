require 'acceptance_helper'

feature 'user can login', %q{
  User took autorelease address uf user.address.nil
  User can to release address
} do

  let(:user) { create(:user) }

  context 'Addresses is empty', :js do
    scenario 'User took error' do
      visit_user(user)
      sleep 1
      expect(page).to have_content t('address_error', minutes: 12)
    end
  end

  context 'Address is not empty', :js do
    let!(:addresses) { create_list(:address, 2) }

    scenario 'User see address' do
      visit_user(user)
      expect(page).to have_content user.address.address
    end

    scenario 'User took success' do
      visit_user(user)
      old_address ||= addresses.first.address
      expect(page).to have_content old_address

      click_on t('generate_address')
      sleep 1
      expect(page).to have_content t('address_success')
      expect(page).to have_content user.address.address
    end

    scenario 'User took error' do
      visit_user(user)
      2.times { click_on t('generate_address') }
      sleep 1
      expect(page).to have_content t('address_error', minutes: 12)
    end
  end
end
