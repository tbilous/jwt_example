require 'acceptance_helper'

feature 'user can login', %q{
  User can to Sign in
} do

  context 'user is created' do
    let(:user) { create(:user) }

    scenario 'Sign In' do
      visit new_user_session_path

      fill_in 'user_email', with: user.email
      fill_in 'user_password', with: user.password

      click_on t('devise.sign_in')

      expect(current_path).to eq my_address_path
    end
  end
end
