module FeatureMacros
  def login_as(user)
    visit new_user_session_path
    fill_in 'Email', with: user.email
    fill_in 'Password', with: user.password
    click_button t('devise.sign_in')
  end

  def visit_user(user)
    login_as(user)
    visit addresses_path
  end

  def visit_quest
    visit root_path
  end
end
