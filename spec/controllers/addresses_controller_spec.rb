require 'rails_helper'
RSpec.describe AddressesController, type: :controller do
  include_context 'users'

  describe 'GET #index' do
    let!(:address) { create(:address, user_id: user.id) }

    it_behaves_like 'unauthorized user' do
      it 'returns 302' do
        get :index
        expect(response).to have_http_status(302)
      end
    end

    it_behaves_like 'authorized user' do
      it 'returns success' do
        get :index
        expect(response).to have_http_status(:success)
      end
    end

    context 'when the request with NO authentication header' do
      let(:authenticated_header) do
        { 'Authorization': 'Bearer 123' }
      end
      it 'should return unauth for retrieve current user info before login' do
        request.headers.merge! authenticated_header
        get :index, format: :json
        expect(response).to have_http_status(:unauthorized)
      end
    end

    context 'when the request contains an authentication header' do
      let(:authenticated_header) do
        token = JwtService.encode(payload: { sub: user.id })
        { 'Authorization': "Bearer #{token}" }
      end

      it 'should return the user address info' do
        request.headers.merge! authenticated_header
        get :index, format: :json
        expect(response).to have_http_status(:success)
        expect(response.body).to be_json_eql(address.address.to_json).at_path('address/address')
      end
    end
  end

  describe 'POST #create' do
    subject { post :create }

    it_behaves_like 'unauthorized user' do
      it 'returns 302' do
        get :index
        expect(response).to have_http_status(302)
      end
    end

    it_behaves_like 'authorized user' do
      context 'when no have addresses' do
        before { subject }
        it 'returns error message and error status' do
          expect(response).to have_http_status(:unprocessable_entity)
          expect(response.body).to be_json_eql(nil.to_json).at_path('address')
          expect(response.body).to be_json_eql(t('address_error', minutes: 12).to_json).at_path('message')
        end
      end
      context 'when user have address and pool not have free addresses' do
        let!(:address) { create(:address, user_id: user.id) }
        before { subject }

        it 'returns error status, error message and error status, address' do
          expect(response).to have_http_status(:unprocessable_entity)
          expect(response.body).to be_json_eql(address.address.to_json).at_path('address/address')
          expect(response.body).to be_json_eql(t('address_error', minutes: 12).to_json).at_path('message')
        end
      end
      context 'when user have address and pool have free addresses' do
        let!(:address) { create(:address, user_id: user.id) }
        let!(:addresses) { create_list(:address, 3) }

        before { subject }
        it 'returns success status, success message, address' do
          expect(response).to have_http_status(:success)
          expect(response.body).to be_json_eql(addresses.first.address.to_json).at_path('address/address')
          expect(response.body).to be_json_eql(t('address_success').to_json).at_path('message')
        end
      end
    end
  end
end
