require 'rails_helper'

RSpec.describe AddAddressesJob, type: :job do
  include_context 'users'
  describe 'create pool of addresses email' do
    around { |example| perform_enqueued_jobs(&example) }
    before { AddAddressesJob.perform_later }

    context 'if Address empty' do
      it '10 addresses created ' do
        expect(Address.count).to eq 10
      end
    end

    context 'if one address had assigned' do
      let!(:address) { create(:address, user_id: user.id) }

      before do
        Address.last.update(user_id: user.id)
      end
      it 'addresses added' do
        expect(Address.where(user_id: nil).count).to eq 10
        expect(Address.count).to eq 11
      end
    end
  end
end
