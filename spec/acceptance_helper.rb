require 'rails_helper'
# require 'rack_session_access/capybara'
# require 'puma'
require 'capybara/email/rspec'
require 'i18n'
require 'capybara/webkit'

RSpec.configure do |config|
  include ActionView::RecordIdentifier
  config.include AcceptanceHelper, type: :feature
  config.include FeatureMacros, type: :feature

  Capybara.server_host = 'lvh.me'
  Capybara.default_host = "http://#{Capybara.server_host}"
  Capybara.server_port = 4100 + ENV['TEST_ENV_NUMBER'].to_i
  Capybara.default_max_wait_time = 2
  Capybara.save_path = './tmp/capybara_output'
  Capybara.always_include_port = true # for correct app_host

  # Capybara.javascript_driver = :webkit

  Capybara::Webkit.configure do |webkit|
    webkit.allow_url('lvh.me')
    webkit.allow_url('fonts.gstatic.com')
    webkit.allow_url('fonts.googleapis.com')
    webkit.allow_url('maxcdn.bootstrapcdn.com')
  end

  require 'selenium/webdriver'

  Capybara.register_driver :chrome do |app|
    Capybara::Selenium::Driver.new(app, browser: :chrome)
  end

  Capybara.register_driver :headless_chrome do |app|
    capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(
      chromeOptions: { args: %w(headless disable-gpu) }
    )

    Capybara::Selenium::Driver.new app,
                                   browser: :chrome,
                                   desired_capabilities: capabilities
  end

  Capybara.javascript_driver = :headless_chrome

  config.use_transactional_fixtures = false

  config.before(:suite) { DatabaseCleaner.clean_with :truncation }

  config.before(:each) { DatabaseCleaner.strategy = :transaction }

  config.before(:each, js: true) { DatabaseCleaner.strategy = :truncation }

  # config.before(:each, type: :feature) { Capybara.app_host = "http://#{Capybara.server_host}" }

  config.before(:each) { DatabaseCleaner.start }

  config.append_after(:each) do
    DatabaseCleaner.clean
  end
end
