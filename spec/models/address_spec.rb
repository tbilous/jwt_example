require 'rails_helper'

RSpec.describe Address, type: :model do
  let(:address) { create(:address) }

  it { should validate_uniqueness_of(:user_id) }

  describe 'generate address value' do
    it 'the address value is corresponding to the required values' do
      expect(address.address).to_not be_nil
      expect(address.address.length).to eq 34
      %w(O 0 I i).each do |i|
        expect(address.address.include?(i)).to be_falsey
      end
    end
  end
end
