class CreateAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :addresses do |t|
      t.string :address, null: false
      t.integer :user_id

      t.timestamps
    end
    add_index :addresses, :user_id, unique: true
  end
end
