class AddAddressesJob < ApplicationJob
  queue_as :default

  def perform
    time = Address.where(user_id: nil).count
    iteration = Rails.application.secrets.pool_count - time
    iteration.times { Address.create } if iteration <= Rails.application.secrets.pool_count
  end
end
