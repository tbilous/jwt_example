module ApplicationHelper
  # Render flash massage  vis js
  def render_flash
    javascript_tag('App.flash = JSON.parse(' "'#{j({ success: flash.notice, error: flash.alert }.to_json)}'" ');')
  end

  # Render partial helper
  # @param partial name
  # @example
  #   component('my_partial')
  # @return rendered partials components/my/_partial
  def component(component_name, locals = {}, &block)
    name = component_name.split('_').first
    render("components/#{name}/#{component_name}", locals, &block)
  end

  # Render partial helper with controller name in path
  # @param partial name
  # @example
  #   controller_component('my_partial')
  # @return rendered partials components/controller_name/_partial
  def controller_component(component_name, locals = {}, &block)
    render("components/#{controller_name}/#{component_name}", locals, &block)
  end

  alias c component
  alias c_c component
end
