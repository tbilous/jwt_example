class Address < ApplicationRecord
  belongs_to :user, optional: true
  validates_uniqueness_of :user_id, allow_nil: true, allow_blank: true
  before_create :set_address_value

  private

  # Assign address value
  # @note run as callback from before_create
  # @return [String]
  # @see Address.generate_address
  def set_address_value
    self.address = generate_address
  end

  # Prepare address value
  # @return [String]
  # @see Address.set_address_value
  def generate_address
    arr = [('a'..'z'), ('A'..'Z'), (0..9)].map(&:to_a).flatten
    filtered = arr.reject { |x| ['O', 0, 'I', 'i'].include?(x) }
    (0...34).map { filtered[rand(filtered.length)] }.join
  end
end
