require 'application_responder'

class ApplicationController < ActionController::Base
  self.responder = ApplicationResponder
  respond_to :html

  protect_from_forgery with: :exception
  prepend_view_path Rails.root.join('frontend')

  before_action :gon_address, unless: :devise_controller?

  private

  def after_sign_in_path_for(_resource)
    my_address_path
  end

  def gon_address
    gon.address = if current_user && !current_user.address.nil?
                    current_user.address.address
                  else
                    0
                  end
  end
end
