class AddressesController < ApplicationController
  before_action :authenticate_user!

  respond_to :json
  # GET /addresses
  # GET /addresses.json
  def index
    respond_with @address = current_user.address
  end

  # POST /addresses.json
  def create
    build_address
  end

  private

  # Call AddressService for build address for user
  # @see AddressService
  def build_address
    status, result = AddressService.new(current_user.id).build_address
    send_response(status, result)
  end

  # Render response for #create
  # @return [Hash]
  # @example Response
  #    {
  #     address: {
  #       address: 'smcWD32832UF2I23J928DFJ92' # OR null
  #     },
  #     message: 'message'
  #   }
  # @see AddressesController.build_address
  def send_response(status, result)
    address = Address.find_by_user_id current_user.id
    if status
      render json: { address: address, message: result }, status: 200
    else
      render json: { address: address, message: result }, status: 422
    end
  end
end
