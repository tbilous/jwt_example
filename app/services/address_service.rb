class AddressService
  def initialize(user_id)
    @user = User.find_by_id user_id
    @address = @user.address
    @free_addresses ||= Address.where(user_id: nil)
  end

  # Entry point
  # @return [Array] [boolean, string]
  # @see Address
  def build_address
    destroy_old_address unless @address.nil?
    create_new_address
  end

  private

  # Destroy old user address if we have free addresses
  # @see AddressService.build_address
  def destroy_old_address
    @address.destroy! if @free_addresses.exists?
  end

  # Assign new address for user
  # @see AddressService.build_address
  def create_new_address
    error = nil

    return send_response(error) if @free_addresses.nil?

    address = @free_addresses.first
    if address.nil?
      error = I18n.t('address_error', minutes: time_to_create)
    else
      address.lock!
      address.update!(user_id: @user.id)
      @address = address
    end
    send_response(error)
  end

  # Check time for new pool generation
  # @see AddressService.build_address
  def time_to_create
    Address.exists? ? calculate_time(Address.last.created_at) : Rails.application.secrets.pool_time
  end

  # Calculate time
  # me for new pool generation
  # @see AddressService.time_to_create
  def calculate_time(time)
    Rails.application.secrets.pool_time - ((Time.current - time) / 60).to_i
  end

  # Return result
  # @see AddressService.build_address
  def send_response(error)
    error ? [false, error] : [true, I18n.t('address_success')]
  end
end
