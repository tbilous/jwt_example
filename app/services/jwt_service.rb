class JwtService
  # Encode params to token
  # @params payload: { sub: 2554545544 }
  # @return [String]
  # @example Response  "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIyNTU0NTQ1NTQ0In0.AxwaTegdhxctdGFjplkC7UTbZuzioeRwH2vLOQZ7D6w"
  # @see Devise::Strategies::JWT
  def self.encode(payload:)
    JWT.encode(payload, secret)
  end

  # Decode token from params
  # @params payload: token: "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIyNTU0NTQ1NTQ0In0.AxwaTegdhxctdGFjplkC7UTbZuzioeRwH2vLOQZ7D6w"
  # @return [Hash]
  # @example Response {"sub"=>"2554545544"}
  # @see Devise::Strategies::JWT
  def self.decode(token:)
    JWT.decode(token, secret).first
  end

  def self.secret
    Rails.application.secrets.secret_key_base
  end
end
