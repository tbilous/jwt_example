class AddressSerializer < ActiveModel::Serializer
  attributes :user_id, :address
end
