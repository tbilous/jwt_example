source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.5.0'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'devise'
gem 'devise-bootstrap-views', '~> 1.0'
gem 'foreman', '0.64.0'
gem 'jbuilder', '~> 2.5'
gem 'pg'
gem 'puma', '~> 3.11'
gem 'rails', '5.2.0'
gem 'webpacker'
# Use Redis adapter to run Action Cable in production
gem 'gon'
gem 'redis', '~> 4.0'
gem 'whenever', require: false
# Use ActiveStorage variant
# gem 'mini_magick', '~> 4.8'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.1.0', require: false

gem 'active_model_serializers'
gem 'font-awesome-rails', '~> 4.7', '>= 4.7.0.4'
gem 'jwt'
gem 'oj'
gem 'oj_mimic_json'
gem 'responders'

group :development, :test do
  gem 'factory_bot_rails', '4.8.2'
  gem 'faker'
  gem 'launchy'
  gem 'pry-byebug'
  gem 'pry-rails'
  gem 'pry-stack_explorer'
  gem 'rubocop', require: false
end

group :development do
  gem 'better_errors', '2.4.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'web-console', '>= 3.3.0'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'yard', require: false
end

group :test do
  gem 'capybara', '3.2.1'
  gem 'capybara-email', '3.0.1'
  gem 'capybara-webkit', github: 'thoughtbot/capybara-webkit', branch: 'master'
  gem 'database_cleaner', '~> 1.7'
  gem 'json_spec'
  gem 'poltergeist'
  gem 'rack_session_access', '0.2.0'
  gem 'rails-controller-testing', '1.0.2'
  gem 'rspec-rails', '~> 3.7', '>= 3.7.2'
  gem 'selenium-webdriver'
  gem 'shoulda-matchers', '~> 3.1', '>= 3.1.2'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
