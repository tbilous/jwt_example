import I18n from "i18n-js";
import "bootstrap/dist/js/bootstrap";
import "components/addresses/addresses";
import "./application.css";
import "./utils.coffee";

import translations from "./translations";

I18n.locale = document.documentElement.lang; // or window.locale
I18n.translations = translations;
