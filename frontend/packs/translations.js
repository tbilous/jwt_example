/* eslint-disable object-shorthand */
import merge from "lodash/merge";
import { en } from "../../config/locales/en.yml";
import * as enResponders from "../../config/locales/responders.en.yml";

export default {
  en: merge(en, enResponders.en)
};
