import jQuery from "jquery";

const container = document.getElementById("address");
const button = document.getElementById("refresh_address_btn");

function displaySuccess(message) {
  window.App.utils.successMessage(message);
}

function displayError(message) {
  window.App.utils.errorMessage(message);
}

function updateAddress(data) {
  displaySuccess(data.message);
  container.innerHTML = data.address.address;
  window.gon.address = data.address.address;
}

function getAddressData() {
  jQuery.ajax({
    type: "POST",
    url: "/addresses.json",
    headers: {
      "Content-Type": "application/json",
      "X-CSRF-Token": button.dataset.token
    },
    success: data => updateAddress(data),
    error: data => displayError(data.responseJSON.message)
  });
}

document.addEventListener("turbolinks:load", () => {
  if (container) {
    if (window.gon.address === 0) {
      getAddressData();
    }
  }
});

button.onclick = () => {
  getAddressData();
};
