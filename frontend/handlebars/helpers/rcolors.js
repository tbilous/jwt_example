const _ = require("lodash");

module.exports = () => _.sample(["pink", "green", "orange"]);
