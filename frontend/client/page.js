import createChannel from "client/cable";

let callback; // declaring a variable that will hold a function later

const chat = createChannel("PageChannel", {
  connected() {
    console.log("Connected");
  },
  received({ message }) {
    if (callback) callback.call(null, message);
  }
});

// Sending a message: "perform" method calls a respective Ruby method
// defined in chat_channel.rb. That's your bridge between JS and Ruby!
function sendMessage(message) {
  chat.perform("send_message", { message });
}

function setCallback(fn) {
  callback = fn;
}

export { sendMessage, setCallback };
