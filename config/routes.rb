Rails.application.routes.draw do
  devise_for :users
  resources :addresses, only: %i(index create)
  match '/my_address', to: 'addresses#index', via: [:get]

  get 'pages/home'
  root to: "addresses#index"
end
