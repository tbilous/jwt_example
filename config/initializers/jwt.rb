module Devise
  module Strategies
    class JWT < Base
      def valid?
        request.headers['Authorization'].present?
      end

      def authenticate!
        payload = JwtService.decode(token: token)
        success! User.find(payload['sub'])
      rescue ::JWT::ExpiredSignature
        fail! I18n.t('devise.jwt.errors.token_expired')
      rescue ::JWT::DecodeError
        fail! I18n.t('devise.jwt.errors.token_invalid')
      rescue StandardError
        fail!
      end

      private

      def token
        request.headers.fetch('Authorization', '').split(' ').last
      end
    end
  end
end
Warden::Strategies.add(:jwt, Devise::Strategies::JWT)
