const { environment } = require("@rails/webpacker");
// eslint-disable-next-line import/no-extraneous-dependencies
const webpack = require("webpack");
const coffee = require("./loaders/coffee");
const handlebars = require("./loaders/handlebars");

environment.loaders.append("coffee", coffee);
environment.loaders.append("handlebars", handlebars);
environment.plugins.prepend(
  "Provide",
  new webpack.ProvidePlugin({
    $: "jquery",
    jQuery: "jquery",
    jquery: "jquery",
    "window.Tether": "tether",
    Popper: ["popper.js", "default"]
  })
);

const config = environment.toWebpackConfig();

config.resolve.alias = {
  jquery: "jquery/src/jquery"
};

module.exports = environment;
