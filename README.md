# README

Things you may want to cover:

* Ruby version 2.5.0, Rails version 5.2.0
* System dependencies: 
  * chromedriver for capybara tests
  * pls add to secrets.yml
    ```
    shared:
      pool_time: 12
      pool_count: 10
     ```
* Start develop `foreman start -f Procfile.dev`, open in browser http://lvh.me:3000
* Start tests `rspec/spec`
